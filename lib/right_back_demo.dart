import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'details.dart';
/**
 *
 *  创建人:xuqing
 *  创建时间：2020年10月16日22:47:31
 *  类说明：侧滑返回
 *
 *
 *
 */

class RightBackDemo extends StatelessWidget {
  RightBackDemo({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return CupertinoPageScaffold(
      child: Center(
        child: CupertinoButton(
          child: Text("跳转详情页",style: TextStyle(
              fontSize: 20,color: Colors.white
          ),),
          color: CupertinoColors.destructiveRed,
          onPressed: (){
            Navigator.of(context).push(CupertinoPageRoute(builder: (BuildContext context){
              return Details();
            }));
          },
        ),
      ),
    );
  }


}
