import 'package:flutter/material.dart';


/**
 *
 * 创建人：xuqing
 * 创建时间：2020年10月16日23:02:17
 * 类说明：详情页面
 *
 *
 */


class Details extends StatefulWidget {
  Details({Key key}) : super(key: key);

  @override
  _DetailsState createState() {
    return _DetailsState();
  }
}

class _DetailsState extends State<Details> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("我是详情页面"),
      ),
      body: Container(
        child: Center(
          child: Text("我是详情页面",style: TextStyle(
            fontSize: 20,color: Colors.blue
          ),),
        ),
      ),
    );
  }
}